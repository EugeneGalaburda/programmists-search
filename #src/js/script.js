@@include('animOnScroll.js');

const trigger = document.querySelector('.hero__trigger')

trigger.addEventListener('click', () => {
  const about = document.querySelector('.about');
  about.scrollIntoView({ behavior: 'smooth', block: 'start' });
})

const vacancies = document.querySelectorAll('.vacancies__item');

vacancies.forEach((item) => {
  item.addEventListener('click', () => {
    item.classList.toggle('active');
  });
});






















